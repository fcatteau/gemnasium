FROM golang:1.17-alpine AS analyzer-build

ENV CGO_ENABLED=0

WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS vrange-nuget-build

COPY vrange/nuget/Vrange /app
WORKDIR /app

RUN \
	dotnet restore && \
	dotnet build && \
	dotnet publish -c Release -r linux-musl-x64 --self-contained true -p:PublishSingleFile=true -p:PublishTrimmed=true && \
	cp bin/Release/netcoreapp3.1/linux-musl-x64/publish/Vrange /vrange-linux

FROM node:14-alpine3.12

ENV PYTHON_PIP_VERSION 20.2.4
ENV PYTHON_SETUPTOOLS_VERSION 50.3.2

ARG RETIRE_JS_VERSION
ENV RETIRE_JS_VERSION ${RETIRE_JS_VERSION:-3.0.3}

ARG RETIREJS_JS_ADVISORY_DB_URL="https://raw.githubusercontent.com/RetireJS/retire.js/master/repository/jsrepository.json"
ARG RETIREJS_JS_ADVISORY_DB="/jsrepository.json"

# GEMNASIUM_RETIREJS_JS_ADVISORY_DB is used internally
# to communicate the local path of jsrepository.json to libfinder.
# The GEMNASIUM_ prefix prevents collisions with RETIREJS_JS_ADVISORY_DB
# of the retire.js analyzer.
ENV GEMNASIUM_RETIREJS_JS_ADVISORY_DB $RETIREJS_JS_ADVISORY_DB

COPY vrange /vrange
COPY --from=vrange-nuget-build /vrange-linux /vrange/nuget/vrange-linux

ENV VRANGE_DIR="/vrange"

ARG GEMNASIUM_DB_LOCAL_PATH="/gemnasium-db"
ARG GEMNASIUM_DB_REMOTE_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db.git"
ARG GEMNASIUM_DB_WEB_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db"
ARG GEMNASIUM_DB_REF_NAME="master"

ENV GEMNASIUM_DB_LOCAL_PATH $GEMNASIUM_DB_LOCAL_PATH
ENV GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_REMOTE_URL
ENV GEMNASIUM_DB_WEB_URL $GEMNASIUM_DB_WEB_URL
ENV GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REF_NAME

RUN \
	# gemnasium-db
	apk add --no-cache git && \
	git clone --branch $GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_LOCAL_PATH && \
	\
	# vrange/php dependencies
	apk add --no-cache php7 php7-dom php7-ctype php7-tokenizer php7-xmlwriter php7-xml composer && \
	composer install -d "$VRANGE_DIR/php" && \
	\
	# vrange/gem dependencies
	apk add --no-cache git ruby ruby-json && \
	\
	# vrange/python dependencies
	apk add --no-cache git python3 && \
	python3 -m ensurepip --default-pip && \
	pip install pip==${PYTHON_PIP_VERSION} && \
	pip install setuptools==${PYTHON_SETUPTOOLS_VERSION} && \
	pip install -r "$VRANGE_DIR/python/requirements.txt" && \
	\
	# vrange/npm dependencies
	yarn --cwd "$VRANGE_DIR/npm/" && \
	\
	# vrange/nuget dependencies
	apk add --no-cache libintl && \
	\
	# ensure we have the most recent versions of all installed packages
	apk upgrade --update-cache --available  && \
	\
	# give write access to CA certificates (OpenShift)
	mkdir -p /etc/ssl/certs/ && \
	touch /etc/ssl/certs/ca-certificates.crt && \
	chmod g+w /etc/ssl/certs/ca-certificates.crt && \
        \
	# give write access to vulnerability database (OpenShift)
	chmod -R g+w $GEMNASIUM_DB_LOCAL_PATH && \
	\
	# install retire.js
	npm install -g retire@$RETIRE_JS_VERSION && \
	wget -O $RETIREJS_JS_ADVISORY_DB $RETIREJS_JS_ADVISORY_DB_URL && \
	echo "done"

# set user HOME to a directory where any user can write (OpenShift)
ENV HOME "/tmp"

COPY --from=analyzer-build /analyzer /analyzer

ENTRYPOINT []
CMD ["/analyzer", "run"]
