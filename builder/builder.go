package builder

import (
	"github.com/urfave/cli/v2"
)

// Builder is implemented by what can provide a generate a lock file or dependency graph for project.
type Builder interface {
	Build(string) (string, error) // Build generates an output file for the given input file, and returns its name
}

// Configurable is implemented by builders that can be configured using CLI flags
type Configurable interface {
	Flags() []cli.Flag            // Flags returns the CLI flags that configure the builder
	Configure(*cli.Context) error // Configure configures the builder based on a CLI context
}
