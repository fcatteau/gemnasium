#!/bin/sh

# checkout branch that will be updated in the image's clone
git -C /gemnasium-db checkout $ORIGINAL_REMOTE_STABLE_BRANCH

# create local remote
git clone --branch $ORIGINAL_REMOTE_STABLE_BRANCH https://gitlab.com/gitlab-org/security-products/gemnasium-db $GEMNASIUM_DB_REMOTE_URL

# ensure local remote has all the needed original branches for this test
git -C $GEMNASIUM_DB_REMOTE_URL branch --track $ORIGINAL_REMOTE_OLDER_STABLE_BRANCH "origin/$ORIGINAL_REMOTE_OLDER_STABLE_BRANCH"

# add fake go vuln
mkdir -p $GEMNASIUM_DB_REMOTE_URL/go/gopkg.in/fake-package
cat > $GEMNASIUM_DB_REMOTE_URL/go/gopkg.in/fake-package/fake-vuln.yml << END_YAML
identifier: "fake-vuln"
package_slug: "go/gopkg.in/fake-package"
title: "fake vuln to demonstrate scan time advisory db updates"
description: "fake vuln to demonstrate scan time advisory db updates"
date: "2021-01-29"
pubdate: "2021-01-29"
affected_range: "=v0.0.0-20161208181325-20d25e280405"
fixed_versions:
- "v1.3.7"
affected_versions: "All versions before 1.3.7"
not_impacted: "All versions starting from 1.3.7"
solution: "Upgrade to version 1.3.7 or above."
urls:
- "https://nvd.nist.gov/vuln/detail/fake-vuln"
cvss_v2: "AV:N/AC:M/Au:N/C:P/I:P/A:N"
cvss_v3: "CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N"
uuid: "f8d39948-1678-453e-87f5-854c53bc2d5b"
versions:
- number: "v1.3.7"
  commit:
    tags:
    - "v1.3.7"
    sha: "addc7461c3a90a040e79aa75bfd245107a210245"
    timestamp: "20200503140227"
END_YAML

git -C $GEMNASIUM_DB_REMOTE_URL add go/gopkg.in/fake-package/fake-vuln.yml
git -C $GEMNASIUM_DB_REMOTE_URL commit -m "Add fake vuln"

# create new tag and branch
git -C $GEMNASIUM_DB_REMOTE_URL tag $NEW_TAG_NAME
git -C $GEMNASIUM_DB_REMOTE_URL checkout -b $NEW_BRANCH_NAME
