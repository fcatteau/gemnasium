package conan

import (
	"encoding/json"
	"errors"
	"io"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

var errParsingDependency = errors.New("Unable to parse dependency")

// Document is a Conan lock file
type Document struct {
	Version   string    `json:"version"`
	GraphLock GraphLock `json:"graph_lock"`
}

// GraphLock is the top level container for dependencies
type GraphLock struct {
	Nodes map[string]DependencyInfo `json:"nodes"`
}

// DependencyInfo describes a Conan dependency
type DependencyInfo struct {
	// PackageAndVersion is formatted as <packagename>/<version>@<user>/<channel>
	// For example: poco/1.9.4, openssl/1.1.1g or jerryscript/2.2.0@user/channel
	Ref string `json:"ref"`
}

const supportedFileFormatVersion = "0.4"

// Parse scans a Conan lock file and returns a list of packages
func Parse(r io.Reader) ([]parser.Package, []parser.Dependency, error) {
	document := Document{}
	err := json.NewDecoder(r).Decode(&document)
	if err != nil {
		return nil, nil, err
	}
	if document.Version != supportedFileFormatVersion {
		return nil, nil, parser.ErrWrongFileFormatVersion
	}

	result := []parser.Package{}
	for _, d := range document.GraphLock.Nodes {
		name, version, err := d.packageAndVersion()
		if err != nil {
			continue
		}

		result = append(result, parser.Package{Name: name, Version: version})
	}

	return result, nil, nil
}

func (d DependencyInfo) packageAndVersion() (string, string, error) {
	packageAndUserChannel := strings.Split(d.Ref, "@")

	packageAndVersion := strings.Split(packageAndUserChannel[0], "/")

	if len(packageAndVersion) != 2 {
		return "", "", errParsingDependency
	}

	return packageAndVersion[0], packageAndVersion[1], nil
}

func init() {
	parser.Register("conan", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypeConan,
		Filenames:   []string{"conan.lock"},
	})
}
