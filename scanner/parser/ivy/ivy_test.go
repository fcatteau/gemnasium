package sbt

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/testutil"
)

func TestParse(t *testing.T) {
	types := []string{"small", "big", "duplicates"}
	for _, tc := range types {
		t.Run(tc, func(t *testing.T) {
			fixture := testutil.Fixture(t, tc, "ivy-report.xml")
			pkgs, _, err := Parse(fixture)
			require.NoError(t, err)

			t.Run("packages", func(t *testing.T) {
				testutil.RequireExpectedPackages(t, tc, pkgs)
			})
		})
	}
}

func TestVersionParsing(t *testing.T) {
	cases := []struct {
		casename string
		xmlstr   string
		err      error
	}{
		{
			casename: "exact version",
			xmlstr:   `<ivy-report version="1.0"></ivy-report>`,
			err:      nil,
		},
		{
			casename: "mismatch on minor version",
			xmlstr:   `<ivy-report version="1.1"></ivy-report>`,
			err:      parser.ErrWrongFileFormatVersion,
		},
		{
			casename: "mismatch on major version",
			xmlstr:   `<ivy-report version="2.0"></ivy-report>`,
			err:      parser.ErrWrongFileFormatVersion,
		},
	}
	for _, c := range cases {
		t.Run(c.casename, func(t *testing.T) {
			_, _, err := Parse(strings.NewReader(c.xmlstr))
			require.Equal(t, c.err, err)
		})
	}
}
