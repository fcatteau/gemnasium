package mvnplugin

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/testutil"
)

func TestMvnplugin(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		tcs := []struct {
			name    string // test case name
			dir     string // dir where fixtures are located
			fixture string // fixture file name
		}{
			{
				name:    "maven simple",
				dir:     "simple",
				fixture: "maven-packages.json",
			},
			{
				name:    "maven big",
				dir:     "big",
				fixture: "maven-packages.json",
			},
			{
				name:    "gradle",
				dir:     "gradle",
				fixture: "gradle-dependencies.json",
			},
		}

		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				fixture := testutil.Fixture(t, tc.dir, tc.fixture)
				got, _, err := Parse(fixture)
				require.NoError(t, err)
				testutil.RequireExpectedPackages(t, tc.dir, got)
			})
		}
	})
}
