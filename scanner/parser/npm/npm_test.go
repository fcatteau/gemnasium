package npm

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/testutil"
)

func TestNpm(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		t.Run("wrong version", func(t *testing.T) {
			fixture := testutil.Fixture(t, "wrong_versions", "package-lock.json")
			_, _, err := Parse(fixture)
			require.EqualError(t, err, parser.ErrWrongFileFormatVersion.Error())
		})

		tcs := []struct {
			name            string
			fixtureName     string
			expectationName string
		}{
			{
				"lockfile-v1/simple",
				"simple",
				"simple",
			},
			{
				"lockfile-v1/big",
				"big",
				"big",
			},
			{
				"lockfile-v2/big",
				"big-lockfile-v2",
				"big",
			},
		}
		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				fixture := testutil.Fixture(t, tc.fixtureName, "package-lock.json")
				got, _, err := Parse(fixture)
				require.NoError(t, err)
				testutil.RequireExpectedPackages(t, tc.expectationName, got)
			})
		}
	})
}
