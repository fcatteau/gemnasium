#!/usr/bin/env ruby
# frozen_string_literal: true

require 'json'

EXIT_CODE_BAD_ARGS = 2

if ARGV.size != 1
  filename = File.basename($PROGRAM_NAME)
  puts <<~EOS
    SYNOPSIS
      #{filename} - evaluate versions and ranges

    SYNOPSIS
      ./#{filename} <queries>

    DESCRIPTION
      Evaluate a JSON document containing versions and version ranges (queries)
      and generate a JSON document telling if the version is in range.
  EOS
  exit EXIT_CODE_BAD_ARGS
end

def eval_queries(queries)
  queries.map do |query|
    eval_query query
  end
end

def eval_query(query)
  version = parse_version query['version']
  parse_range(query['range']).each do |req|
    return query.merge("satisfies": true) if req.satisfied_by?(version)
  end
  query.merge("satisfies": false)
rescue StandardError => e
  query.merge("error": e.to_s)
end

def parse_version(version)
  raise 'Missing version' if version.nil? || version == ''

  Gem::Version.new version
end

def parse_range(range)
  raise 'Missing requirement' if range.nil? || range == ''

  range.split('||').map do |requirements|
    Gem::Requirement.new(requirements.strip.split(/\s+/).map(&:strip))
  end
end

input_file = ARGV[0]

begin
  json_object = JSON.parse(File.read(input_file))
  if json_object.instance_of?(Array)
    puts JSON.pretty_generate eval_queries json_object
  else
    puts "JSON Array expected"
    exit(1)
  end
rescue StandardError => e
  puts e
  exit(1)
end
